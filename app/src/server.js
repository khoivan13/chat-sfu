const Room = require("./room");
const roomManagers = {};

const io = require("socket.io")(process.argv[2] || 3000, {
  cors: {
    origin: "*",
  },
});
io.use(function (socket, next) {
  const bearer = socket.request.headers["authorization"];
  let token;
  if (bearer && bearer.startsWith("Bearer ")) {
    token = JSON.parse(bearer.substring(7));
    if (token) {
      socket.userId = token.id;
      return next();
    }
  }
  return next();
  // next(new Error("unauthenticated"));
});

io.on("connection", (socket) => {
  console.log("new connect", socket.id);
  socket.on("joinRtcRoom", (roomId) => {
    let room = roomManagers[roomId];
    if (!room) {
      room = new Room(roomId);
      roomManagers[roomId] = room;
    }
    room.join(socket);
  });
});

console.log("Server start at port", process.argv[2]);
