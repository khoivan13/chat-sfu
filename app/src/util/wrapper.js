class SubscribeWrapper {
  constructor(socket) {
    socket.setMaxListeners(0);
    this.socket = socket;
    this.subscriptions = [];
  }

  on(key, callback) {
    let newCallback = (response) => callback(response);
    this.socket.on(key, newCallback);
    return this.subscriptions.push({ key: key, callback: newCallback }) - 1;
  }

  off(i) {
    this.socket.removeListener(
      this.subscriptions[i].key,
      this.subscriptions[i].callback
    );
  }

  removeAll() {
    for (let sub of this.subscriptions) {
      this.socket.removeListener(sub.key, sub.callback);
    }
  }
}

module.exports = SubscribeWrapper;
