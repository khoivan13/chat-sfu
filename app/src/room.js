const { RTCPeerConnection } = require("../../packages/webrtc/src");
const SubscribeWrapper = require("./util/wrapper");

class Room {
  constructor(id) {
    this._id = id;
    this._publishers = {};
    this._peerManager = {};
    this._connected = {};
    this._trackSenders = {};
  }

  join(socket) {
    socket.sub = new SubscribeWrapper(socket);
    socket.join(this._id);
    this._connect(socket);
  }

  async _connect(socket) {
    console.log("_connect", socket.id);
    const pc = new RTCPeerConnection({});
    socket.pc = pc;

    pc.onconnectionstatechange = () => {
      if (pc.iceConnectionState == "connected") {
        this._connected[socket.id] = socket;
      }
    };

    pc.addTransceiver("audio", {
      direction: "sendonly",
    });

    const filtered = Object.keys(this._publishers)
      .filter((key) => key != socket.id)
      .reduce((obj, key) => {
        obj[key] = this._publishers[key];
        return obj;
      }, {});
    const trackList = Object.values(filtered).flat();
    for (let track of trackList) {
      this._onAddTrack(socket, track.track, {
        from: this._connected[track.socId],
      });
    }

    pc.createOffer()
      .then((offer) => {
        return pc.setLocalDescription(offer);
      })
      .then(() => {
        // const sdp = pc.localDescription.sdp.replace(
        //   new RegExp("a=msid:(.+) ", "g"),
        //   `a=msid:$1 ${socket.userId}-`
        // );
        socket.emit("offer", JSON.stringify({ sdp: pc.localDescription }));
      })
      .catch(console.log);

    socket.sub.on("publish", async () => {
      console.log(socket.id, "publish");
      const transceiver = pc.addTransceiver("audio", {
        direction: "recvonly",
      });
      transceiver.metaId = socket.userId;
      transceiver.onTrack.subscribe((track) => {
        {
          // VIDEO
          // track.onReceiveRtp.once((rtp) => {
          //   setInterval(() => {
          //     transceiver.receiver.sendRtcpPLI(rtp.header.ssrc);
          //   }, 2000);
          // });
        }
        if (!this._publishers[socket.id]) {
          this._publishers[socket.id] = [{ track, socId: socket.id }];
        } else {
          this._publishers[socket.id].push({ track, socId: socket.id });
        }

        const _connectedFilter = Object.keys(this._connected).filter(
          (key) => key != socket.id
        );
        _connectedFilter.forEach((socketId) => {
          this._onAddTrack(socketId, track, {
            from: socket,
            completed: (connection) => {
              const { pc } = connection;
              pc.createOffer()
                .then((offer) => {
                  return pc.setLocalDescription(offer);
                })
                .then(() => {
                  // const sdp = pc.localDescription.sdp.replace(
                  //   new RegExp("a=msid:(.+) ", "g"),
                  //   `a=msid:$1 ${socket.userId}-`
                  // );
                  connection.emit(
                    "offer",
                    JSON.stringify({
                      sdp: pc.localDescription,
                    })
                  );
                })
                .catch(console.log);
            },
          });
        });
      });

      pc.createOffer()
        .then((offer) => {
          return pc.setLocalDescription(offer);
        })
        .then(() => {
          socket.emit("offer", JSON.stringify({ sdp: pc.localDescription }));
        })
        .catch(console.log);
    });

    socket.sub.on("unpublish", async (data) => {
      const { media } = JSON.parse(data);
      try {
        const transceiver = pc.getTransceivers().find((t) => t.mid === media);
        pc.removeTrack(transceiver.sender);
        pc.createOffer()
          .then((offer) => {
            return pc.setLocalDescription(offer);
          })
          .then(() => {
            socket.emit("offer", JSON.stringify({ sdp: pc.localDescription }));
          })
          .catch(console.log);
      } catch (e) {
        console.log("unpublish.ERR", e);
      }
    });

    socket.sub.on("answer", async (data) => {
      try {
        const { sdp } = JSON.parse(data);
        await pc.setRemoteDescription(sdp);
      } catch (e) {
        console.log(socket.id);
        console.log("answer.ERR", e);
      }
    });

    socket.sub.on("disconnect", () => {
      console.log("DISCONNECT");
      this._onDisconnect(socket);
    });
  }

  //: "sendonly" recvonly

  async _onAddTrack(socket, { id, kind }, { from, completed = () => {} } = {}) {
    try {
      if (typeof socket == "string") {
        socket = this._connected[socket];
      }
      const { pc } = socket;

      if (["closed", "failed"].includes(pc.iceConnectionState)) {
        return;
      }
      const filtered = Object.keys(this._publishers)
        .filter((key) => key != socket.id)
        .reduce((obj, key) => {
          obj[key] = this._publishers[key];
          return obj;
        }, {});

      const trackObj = Object.values(filtered)
        .flat()
        .find((item) => {
          return item.track.id == id && item.track.kind == kind;
        });

      if (!trackObj) {
        console.log("no publisher");
        return;
      }

      const transceiver = pc.addTransceiver("audio", {
        direction: "sendonly",
      });

      if (from) {
        transceiver.metaId = from.userId;
      }
      await transceiver.sender.replaceTrack(trackObj.track);
      console.log(trackObj.track.id);
      this._trackSenders[trackObj.track.id + "_" + socket.id] =
        transceiver.sender;

      completed(socket);
    } catch (e) {
      console.log("_onAddTrack.ERR", e);
    }
  }

  async _onRemoveTrack(socketid, tracks) {
    const socket = this._connected[socketid];
    const { pc } = socket;
    for (let trackObj of tracks) {
      const sender = this._trackSenders[trackObj.track.id + "_" + socketid];
      if (sender && !sender.stopped) {
        console.log("_onRemoveTrack", socketid);
        pc.removeTrack(sender);
      }
    }

    pc.createOffer()
      .then((offer) => {
        return pc.setLocalDescription(offer);
      })
      .then(() => {
        socket.emit("offer", JSON.stringify({ sdp: pc.localDescription }));
      })
      .catch(console.log);
  }

  async _onDisconnect(socket) {
    if (socket.sub) {
      socket.sub.removeAll();
      socket.sub = null;
    }
    if (this._publishers[socket.id]) {
      const tracks = this._publishers[socket.id];
      const socketKeys = Object.keys(this._connected);
      for (let socketid of socketKeys) {
        if (socketid != socket.id) {
          this._onRemoveTrack(socketid, tracks);
        }
      }
      // push remove stream;
      delete this._publishers[socket.id];
    }

    if (socket.pc) {
      await socket.pc.close();
    }

    delete this._connected[socket.id];
  }
}

module.exports = Room;
